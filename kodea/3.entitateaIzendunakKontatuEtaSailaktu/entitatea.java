
public class entitatea {
	
	private String izena;
	private boolean lema;
	private int kop;
	private String type;
	private String gaia;
	private String urtea;

	
	entitatea(String izena, boolean b, String t, String gaia, String urtea)
	{
		this.izena = izena;
		this.lema = b;
		this.kop = 1;
		this.type = t;
		this.gaia = gaia;
		this.urtea = urtea;

	}
	public String getIzena() {
		return izena;
	}
	public void setIzena(String izena) {
		this.izena = izena;
	}
	public boolean isLema() {
		return lema;
	}
	public void setLema(boolean lema) {
		this.lema = lema;
	}
	public int getKop() {
		return kop;
	}
	public void setKop(int kop) {
		this.kop = kop;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getGaia() {
		return gaia;
	}
	public void setGaia(String gaia) {
		this.gaia = gaia;
	}
	public String getUrtea() {
		return urtea;
	}
	public void setUrtea(String urtea) {
		this.urtea = urtea;
	}


}
