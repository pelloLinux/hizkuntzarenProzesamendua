

public class ordenatzeAlgoritmoak {

	public ordenatzeAlgoritmoak(){}
	
    public void quickSort(int lowerIndex, int higherIndex, entitatea[] array) {
        
        int i = lowerIndex;
        int j = higherIndex;
        // calculate pivot number, I am taking pivot as middle index number
        int pivot = array[lowerIndex+(higherIndex-lowerIndex)/2].getKop();
        // Divide into two arrays
        while (i <= j) {
            /**
             * In each iteration, we will identify a number from left side which
             * is greater then the pivot value, and also we will identify a number
             * from right side which is less then the pivot value. Once the search
             * is done, then we exchange both numbers.
             */
            while (array[i].getKop() < pivot) {
                i++;
            }
            while (array[j].getKop() > pivot) {
                j--;
            }
            if (i <= j) {
                exchangeNumbers(i, j, array);
                //move index to next position on both sides
                i++;
                j--;
            }
        }
        // call quickSort() method recursively
        if (lowerIndex < j)
            quickSort(lowerIndex, j, array);
        if (i < higherIndex)
            quickSort(i, higherIndex, array);
    }
 
    private void exchangeNumbers(int i, int j, entitatea[] array) {
        entitatea temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
