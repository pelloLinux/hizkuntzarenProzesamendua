import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;


public class nagusia {


	static Map<String, entitatea> entitateak = new HashMap<String, entitatea>();
	static LinkedList<String> entitateenIzenak = new LinkedList<String>();

	public static void main(String[] args) throws FileNotFoundException {

		LinkedList<String> izenak = fitxategienIzenakLortu();
		LinkedList<entitateProbisionalak> momentukoEntitateak = new LinkedList<entitateProbisionalak>();
		//LinkedList<entitatea> entitateak = new LinkedList<entitatea>();

		String[] type = {"LOC","PER","ORG","denak"};
		String[] gaiak = {"aisia","azoka-herritik","besterik","ekonomia","euskara","festak","gizartea","herria","hezkuntza","hirigintza","iritzia","kirolak","komunikabideak","kultura","politika","publizitatea","taldeak","udala","uztarriaeus","denak"};

		for(String ty : type)
		{
			for(String gai : gaiak)
			{
				System.out.println("***** "+ty+"  "+gai+" *****");
				entitateak = new HashMap<String, entitatea>();
				entitateenIzenak = new LinkedList<String>();

				for(int i=0; i<izenak.size(); i++)
				{
					if(i%100==0)System.out.println(" %"+i*100/izenak.size());
					if(i%10==0)System.out.print("*");

					momentukoEntitateak = entitateakLortu(izenak.get(i));
					//for(int j=0; j<momentukoEntitateak.size(); j++)System.out.println(momentukoEntitateak.get(j).getIzena());
					entitateakSortuEdoSartu(momentukoEntitateak, gai, ty);
				}	
				//quickSort erabiltzeko array batera pasa



				entitatea[] array = new entitatea[entitateak.size()];

				if(entitateak.size()!=0)
				{
					for(int i=0; i<entitateenIzenak.size(); i++)
						array[i]=entitateak.get(entitateenIzenak.get(i));


					ordenatzeAlgoritmoak ord = new ordenatzeAlgoritmoak();
					ord.quickSort(0, array.length-1, array);

				}

				PrintWriter out;
				out = new PrintWriter("../"+ty+"_"+gai+".txt");
				for(int i=0; i<array.length; i++)
					out.println(array[i].getIzena()+"="+array[i].getKop());
				out.close();
			}		
		}
	}

	public static LinkedList<String> fitxategienIzenakLortu() throws FileNotFoundException
	{
		Scanner s = new Scanner(new File("fitxategienIzenak.txt"));
		LinkedList<String> izenak = new LinkedList<String>();

		while(s.hasNextLine())
			izenak.add(s.nextLine());
		s.close();		
		return izenak;
	}

	public static LinkedList<entitateProbisionalak> entitateakLortu(String fitx) throws FileNotFoundException
	{
		Scanner s = new Scanner(new File(fitx));
		String lag;
		LinkedList<entitateProbisionalak> lemakLortzeko = new LinkedList<entitateProbisionalak>();
		LinkedList<entitateProbisionalak> lemakLortzeko2 = new LinkedList<entitateProbisionalak>();
		//LinkedList<String> entitateak = new LinkedList<String>();
		LinkedList<entitateProbisionalak> entitateakPro = new LinkedList<entitateProbisionalak>();

		while(s.hasNextLine())
		{
			lag = s.nextLine();
			if(lag.contains("<entity id"))
			{
				String type = lag.split("type=\"")[1].split("\"")[0];
				s.nextLine();
				lag = s.nextLine();
				lag = lag.split("--")[1];
				
				String gaia = fitx.split("_")[0];

				if(lag.split(" ").length>1)
				{
					//	entitateak.add(lag);
					lemakLortzeko2.add(new entitateProbisionalak(lag, false, type, gaia, fitx));

				}else
					lemakLortzeko.add(new entitateProbisionalak(lag, true, type, gaia, fitx));
			}
		}
		s.close();
		entitateakPro = lemakLortuEtaErrepikapenakKendu(lemakLortzeko, fitx, entitateakPro);
		entitateakPro = lemakLortuEtaErrepikapenakKendu2(lemakLortzeko2, fitx, entitateakPro);
		return entitateakPro;
	}

	public static LinkedList<entitateProbisionalak> lemakLortuEtaErrepikapenakKendu(LinkedList<entitateProbisionalak> entitateak, String fitx, LinkedList<entitateProbisionalak> entPro) throws FileNotFoundException
	{
		Scanner s = new Scanner(new File(fitx));
		String lag;
		LinkedList<String> biAldiz = new LinkedList<String>();

		while(s.hasNextLine())
		{
			if(s.nextLine().contains("<terms>"))
			{
				while(s.hasNextLine())
				{
					lag = s.nextLine();
					if(lag.contains("<entities>"))break;

					for(int i=0; i<entitateak.size(); i++)
					{
						try{
							if(lag.contains(entitateak.get(i).getIzena()))
							{
								lag = s.nextLine();
								lag = lag.split("lemma=\"")[1].split("\"")[0];
								if(!biAldiz.contains(lag)){
									entitateak.get(i).setIzena(lag);
									entPro.add(entitateak.get(i));
									biAldiz.add(lag);
								}
								break;
							}
						}catch (Exception e) {
							//	System.out.println("error");
							continue;
						}
					}
				}
			}
		}
		s.close();
		return entPro;
	}


	public static LinkedList<entitateProbisionalak> lemakLortuEtaErrepikapenakKendu2(LinkedList<entitateProbisionalak> entitateak, String fitx, LinkedList<entitateProbisionalak> entPro) throws FileNotFoundException
	{
		Scanner s = new Scanner(new File(fitx));
		String lag;
		LinkedList<String> biAldiz = new LinkedList<String>();

		while(s.hasNextLine())
		{
			if(s.nextLine().contains("<terms>"))
			{
				while(s.hasNextLine())
				{
					lag = s.nextLine();
					if(lag.contains("<entities>"))break;

					for(int i=0; i<entitateak.size(); i++)
					{
						try{

							String[] banatuta = entitateak.get(i).getIzena().split(" ");
							int kop = banatuta.length;
							String azkena = banatuta[kop-1];
							if(lag.contains(azkena))
							{
								lag = s.nextLine();
								lag = lag.split("lemma=\"")[1].split("\"")[0];
								banatuta[kop-1] = lag;
								lag = "";
								//hitza osatu
								for(int j=0; j<kop; j++)
								{
									lag = lag + banatuta[j]+ " ";
								}

								if(!biAldiz.contains(lag)){
									entitateak.get(i).setIzena(lag);
									entPro.add(entitateak.get(i));
									biAldiz.add(lag);
								}
								break;
							}
						}catch (Exception e) {
							//	System.out.println("error");
							continue;
						}
					}
				}
			}
		}
		s.close();
		return entPro;
	}

	public static void entitateakSortuEdoSartu(LinkedList<entitateProbisionalak> momentukoEntitateak, String g, String t)
	{
		for(entitateProbisionalak ent : momentukoEntitateak)
		{
			if(ent.getType().compareTo(t)==0 || t.compareTo("denak")==0)
			{
				if(ent.getGaia().compareTo(g)==0 || g.compareTo("denak")==0)
				{
					if(entitateak.containsKey(ent.getIzena()))
					{
						entitateak.get(ent.getIzena()).setKop(entitateak.get(ent.getIzena()).getKop()+1);//kopurua bat igo
					}
					else
					{
						String infor = ent.getFitxIzena();
						String gaia = infor.split("_")[0];
						String urtea = infor.split("_")[1];
						entitatea berria = new entitatea(ent.getIzena(), true, ent.getType(),gaia,urtea);
						entitateak.put(ent.getIzena(), berria);
						entitateenIzenak.add(ent.getIzena());
					}
				}
			}

		}
	}

}
