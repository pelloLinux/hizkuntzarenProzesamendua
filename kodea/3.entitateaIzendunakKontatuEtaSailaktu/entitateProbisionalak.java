
public class entitateProbisionalak {
	
	private String izena;
	private boolean lema;
	private String type;
	private String fitxIzena;
	private String gaia;
	
	entitateProbisionalak(String izena, boolean b, String t, String g, String fitx)
	{
		this.izena = izena;
		this.lema = b;
		this.type = t;
		this.setGaia(g);
		this.setFitxIzena(fitx);
	}
	public String getIzena() {
		return izena;
	}
	public void setIzena(String izena) {
		this.izena = izena;
	}
	public boolean isLema() {
		return lema;
	}
	public void setLema(boolean lema) {
		this.lema = lema;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFitxIzena() {
		return fitxIzena;
	}
	public void setFitxIzena(String fitxIzena) {
		this.fitxIzena = fitxIzena;
	}
	public String getGaia() {
		return gaia;
	}
	public void setGaia(String gaia) {
		this.gaia = gaia;
	}

}
