
<html>
	<head>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">


			google.load('visualization','1.0',{'packages':['corechart']});
			google.setOnLoadCallback(dibujar);
	
			function dibujar()
			{
			
					var data = new google.visualization.DataTable();
					data.addColumn('string','Ciudad');
					data.addColumn('number','Agerpen Kopurua');
					data.addRows(
								[
									
['eaj',346],
['Espainiako Auzitegi nazional ',167],
['ertzaintza',85],
['pse-ee',83],
['Urola Kostako hitza ',82],
['Herrialde katalan ',78],
['udalbatzar',76],
['Auzitegi nazional ',65],
['aralar',65],
['Eusko jaurlaritza ',65],
['udal',63],
['epaiketa',56],
['Amnistiaren Aldeko mugimendu ',54],
['Espainiako polizia ',54],
['pp',53],
['Espainiako gobernu ',51],
['hau',48],
['real',48],
['Espainiako Auzitegi goren ',47],
['polizia',44],
['Guardia zibila ',44],
['La santen ',43],
['psoe',41],
['lab',41],
['Urola Kostako hitz ',41],
['Gipuzkoako Foru aldundi ',40],
['Eusko legebiltzar ',38],
['Guardia zibil ',37],
['behar',36],
['Azpeitiko udal ',35],
['udalbiltza',34],
['udaletxe',33],
['Sindikatu zahar ',32],
['Frantziako polizia ',32],
['Azpeitiko Amnistiaren Aldeko mugimendu ',31],
['desegin',30],
['aam',29],
['eh',28],
['Kalera kale ',27],
['eajko',27],
['eta',27],
['hurbilketa',26],
['nuarbe',25],
['espainia',25],
['azpeiti',24],
['Udal gobernu ',24],
['Auzitegi goren ',24],
['gehiago',24],
['Madrilgo Auzitegi nazional ',23],
['behar_izan',22],
['udala',22],
['estatu',22],
['Espainiako justizia ',21],
['Orkatz Kultur elkarte ',21],
['segi',21],
['Puerto santa ',21],
['Donostiako Lurralde auzitegi ',21],
['ertzain',21],
['Frontoi txiki ',20],
['La sante ',19],
['batasuna',19],
['jaurlaritza',18],
['elkarretaratu',18],
['Azpeitiko udaletxe ',18],
['horiek',18],
['Gipuzkoako Batzar nagusi ',17],
['Azpeitiko eaj ',16],
['Sevilla ii ',16],
['Urola kosta ',16],
['_',16],
['fiskaltza',16],
['hori',16],
['kongresu',15],
['dispertsio',15],
['ump',15],
['eae-an',15],
['Europako parlamentu ',15],
['Espainiako kongresu ',14],
['Espainiako estatu ',14],
['eae-anv',14],
['ertzain-etxe',14],
['Urola Kostako lab ',14],
['roblesi',14],
['Gipuzkoako auzitegi ',14],
['Puerto Santa maria ',13],
['Azpeitiko epaitegi ',13],
['senatu',13],
['Foru aldundi ',13],
['jarraitu',13],
								]
								);
var opciones = {'title':'',
									'width':1500*2,
									'height':500};

					var grafica = new google.visualization.ColumnChart(document.getElementById('charts'));
					grafica.draw(data,opciones);

				
		}

		</script>
				<script type="text/javascript">
			
		function bilaketaEgin(){
		
			if(document.getElementById('denakType').checked) {

				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'denakTypeDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'denakTypeAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'denakTypeAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'denakTypeBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'denakTypeEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'denakTypeEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'denakTypeFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'denakTypeGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'denakTypeHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'denakTypeHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'denakTypeHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'denakTypeIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'denakTypeKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'denakTypeKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'denakTypeKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'denakTypePolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'denakTypePublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'denakTypeTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'denakTypeUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'denakTypeUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('loc').checked) {

				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'locDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'locAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'locAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'locBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'locEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'locEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'locFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'locGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'locHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'locHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'locHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'locIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'locKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'locKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'locKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'locPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'locPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'locTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'locUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'locUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('org').checked) {

				
				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'orgDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'orgAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'orgAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'orgBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'orgEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'orgEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'orgFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'orgGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'orgHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'orgHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'orgHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'orgIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'orgKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'orgKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'orgKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'orgPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'orgPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'orgTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'orgUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'orgUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('per').checked) {

				
				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'perTypeDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'perAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'perAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'perBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'perEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'perEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'perFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'perGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'perHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'perHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'perHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'perIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'perKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'perKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'perKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'perPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'perPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'perTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'perUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'perUztarriaeus.php'; 
				}
			}
		}

		</script>

	<title>Uztarriako Entitateen Estadistikak</title>

	</head>
	<body>
<h1 align="center">Uztarriako Entitate Izendunen Grafikoak</h1>
		<p align="center">Uztarriak 2001etik 2017rarte beraien weborrian argitaratutako 17034 artikulu prozesatu dira.<br>
						  Grafikoak, entitate moten arabera eta gaien arabera banatuta daude  </p>
		<hr>
		<input type="radio" id='denakType' name="type" value="1" onclick = "bilaketaEgin()">Denak
		<input type="radio" id='loc' name="type" value="2" onclick = "bilaketaEgin()">Lekuak
		<input type="radio" id='org' name="type" value="3" onclick = "bilaketaEgin()">Antolakundeak
		<input type="radio" id='per' name="type" value="4" onclick = "bilaketaEgin()">Pertsonak<br> 
		<hr>
		<input type="radio" id='denakGaia' name="gaia" value="1" onclick = "bilaketaEgin()">Denak
		<input type="radio" id='aisia' name="gaia" value="1" onclick = "bilaketaEgin()">Aisia
		<input type="radio" id='azoka-herritik' name="gaia" value="2" onclick = "bilaketaEgin()">azoka-herritik
		<input type="radio" id='besterik' name="gaia" value="3" onclick = "bilaketaEgin()">besterik
		<input type="radio" id='ekonomia' name="gaia" value="4" onclick = "bilaketaEgin()">ekonomia
		<input type="radio" id='euskara' name="gaia" value="5" onclick = "bilaketaEgin()">euskara
		<input type="radio" id='festak' name="gaia" value="6" onclick = "bilaketaEgin()">festak
		<input type="radio" id='gizartea' name="gaia" value="7" onclick = "bilaketaEgin()">gizartea
		<input type="radio" id='herria' name="gaia" value="8" onclick = "bilaketaEgin()">herria
		<input type="radio" id='hezkuntza' name="gaia" value="9" onclick = "bilaketaEgin()">hezkuntza<br>
		<input type="radio" id='hirigintza' name="gaia" value="10" onclick = "bilaketaEgin()">hirigintza
		<input type="radio" id='iritzia' name="gaia" value="11" onclick = "bilaketaEgin()">iritzia
		<input type="radio" id='kirolak' name="gaia" value="12" onclick = "bilaketaEgin()">kirolak
		<input type="radio" id='komunikabideak' name="gaia" value="13" onclick = "bilaketaEgin()">komunikabideak
		<input type="radio" id='kultura' name="gaia" value="14" onclick = "bilaketaEgin()">kultura
		<input type="radio" id='politika' name="gaia" value="15" onclick = "bilaketaEgin()">politika
		<input type="radio" id='publizitatea' name="gaia" value="16" onclick = "bilaketaEgin()">publizitatea
		<input type="radio" id='taldeak' name="gaia" value="17" onclick = "bilaketaEgin()">taldeak
		<input type="radio" id='udala' name="gaia" value="18" onclick = "bilaketaEgin()">udala
		<input type="radio" id='uztarriaeus' name="gaia" value="19" onclick = "bilaketaEgin()">uztarriaeus
		<hr>


	 
		<div id='charts'></div>

		
	</body>
</html>
