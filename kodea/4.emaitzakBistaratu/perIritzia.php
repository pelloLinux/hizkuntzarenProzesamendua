
<html>
	<head>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">


			google.load('visualization','1.0',{'packages':['corechart']});
			google.setOnLoadCallback(dibujar);
	
			function dibujar()
			{
			
					var data = new google.visualization.DataTable();
					data.addColumn('string','Ciudad');
					data.addColumn('number','Agerpen Kopurua');
					data.addRows(
								[
									['xabier',32],
['beniemendean_zaion',30],
['franco',24],
['Alfonso etxeberria ',16],
['Gustavo gutiérrez ',15],
['Jon sobrino ',15],
['Juan Inazio hartsuaga ',15],
['madina',15],
['Xabier madina ',15],
['Miguel elizondo ',15],
['Fernando cardenal ',15],
['Valentin Bengoa etxebarria ',15],
['Valentin bengoa ',15],
['bengoa',15],
['jauregi',15],
['ellakuria',15],
['Julian eizmendi ',13],
['Aitor gorrotxategi ',12],
['Pako aristi ',12],
['Eneko etxeberria ',12],
['berria',11],
['ibarretxe',9],
['Jose Luis otamendi ',9],
['Manuel unanue ',8],
['Ana mendizabal ',8],
['bastida',8],
['azpeitia',8],
['Ramon etxezarreta ',7],
['Joxe lizaso ',7],
['inaxio',7],
['Enekoitz esnaola ',6],
['arregi',6],
['Imanol elias ',6],
['Inazio uria ',6],
['gorrotxategi',6],
['Iñaki goenaga ',6],
['aznar',6],
['Iñaki uria ',5],
['imanol',5],
['joxe',5],
['otaegi',5],
['Aitor arangu ',5],
['Miren odriozola ',5],
['Txema auzmendi ',5],
['Inaxio lasa ',5],
['San sansebastiana ',5],
['uranga',4],
['Joxe arregi ',4],
['Xebaxtian lizaso ',4],
['antxieta',4],
['otamendi',4],
['azpeitiarron',4],
['garzon',4],
['Xabier oleaga ',4],
['Martin ugalde ',4],
['jox',4],
['Garikoitz etxeberria ',4],
['Mikel azkune ',4],
['garmendia',4],
['Martxelo otamendi ',4],
['altuna',4],
['Ioritz aizpuru ',4],
['Patxi baztarrika ',4],
['Jabier altuna ',4],
['Iban arregi ',4],
['Xabier euzkitz ',4],
['Mikel ibarzabal ',4],
['Aitor aranguren ',4],
['azpeitiar',4],
['Leo etxeberria ',3],
['Jose Mari izura ',3],
['Sebastian lizaso ',3],
['Mikel arregi ',3],
['zapatero',3],
['Rafael delas ',3],
['Anjel otaegi ',3],
['azkue',3],
['aristi',3],
['euzkitze',3],
['Joxe Luis aizpuru ',3],
['Patxi saez ',3],
['Mikel aranburu ',3],
['arellano',3],
['Imanol lazkano ',3],
['Mikel labaka ',3],
['Laxaro azkun ',3],
['egiguren',3],
['agir',3],
['mendizabal',3],

								]
								);
var opciones = {'title':'',
									'width':1500*2,
									'height':500};

					var grafica = new google.visualization.ColumnChart(document.getElementById('charts'));
					grafica.draw(data,opciones);

				
		}

		</script>
				<script type="text/javascript">
			
		function bilaketaEgin(){
		
			if(document.getElementById('denakType').checked) {

				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'denakTypeDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'denakTypeAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'denakTypeAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'denakTypeBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'denakTypeEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'denakTypeEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'denakTypeFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'denakTypeGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'denakTypeHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'denakTypeHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'denakTypeHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'denakTypeIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'denakTypeKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'denakTypeKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'denakTypeKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'denakTypePolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'denakTypePublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'denakTypeTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'denakTypeUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'denakTypeUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('loc').checked) {

				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'locDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'locAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'locAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'locBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'locEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'locEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'locFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'locGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'locHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'locHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'locHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'locIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'locKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'locKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'locKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'locPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'locPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'locTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'locUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'locUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('org').checked) {

				
				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'orgDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'orgAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'orgAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'orgBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'orgEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'orgEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'orgFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'orgGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'orgHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'orgHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'orgHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'orgIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'orgKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'orgKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'orgKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'orgPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'orgPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'orgTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'orgUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'orgUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('per').checked) {

				
				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'perTypeDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'perAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'perAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'perBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'perEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'perEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'perFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'perGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'perHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'perHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'perHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'perIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'perKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'perKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'perKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'perPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'perPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'perTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'perUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'perUztarriaeus.php'; 
				}
			}
		}

		</script>

	<title>Uztarriako Entitateen Estadistikak</title>

	</head>
	<body>
<h1 align="center">Uztarriako Entitate Izendunen Grafikoak</h1>
		<p align="center">Uztarriak 2001etik 2017rarte beraien weborrian argitaratutako 17034 artikulu prozesatu dira.<br>
						  Grafikoak, entitate moten arabera eta gaien arabera banatuta daude  </p>
		<hr>
		<input type="radio" id='denakType' name="type" value="1" onclick = "bilaketaEgin()">Denak
		<input type="radio" id='loc' name="type" value="2" onclick = "bilaketaEgin()">Lekuak
		<input type="radio" id='org' name="type" value="3" onclick = "bilaketaEgin()">Antolakundeak
		<input type="radio" id='per' name="type" value="4" onclick = "bilaketaEgin()">Pertsonak<br> 
		<hr>
		<input type="radio" id='denakGaia' name="gaia" value="1" onclick = "bilaketaEgin()">Denak
		<input type="radio" id='aisia' name="gaia" value="1" onclick = "bilaketaEgin()">Aisia
		<input type="radio" id='azoka-herritik' name="gaia" value="2" onclick = "bilaketaEgin()">azoka-herritik
		<input type="radio" id='besterik' name="gaia" value="3" onclick = "bilaketaEgin()">besterik
		<input type="radio" id='ekonomia' name="gaia" value="4" onclick = "bilaketaEgin()">ekonomia
		<input type="radio" id='euskara' name="gaia" value="5" onclick = "bilaketaEgin()">euskara
		<input type="radio" id='festak' name="gaia" value="6" onclick = "bilaketaEgin()">festak
		<input type="radio" id='gizartea' name="gaia" value="7" onclick = "bilaketaEgin()">gizartea
		<input type="radio" id='herria' name="gaia" value="8" onclick = "bilaketaEgin()">herria
		<input type="radio" id='hezkuntza' name="gaia" value="9" onclick = "bilaketaEgin()">hezkuntza<br>
		<input type="radio" id='hirigintza' name="gaia" value="10" onclick = "bilaketaEgin()">hirigintza
		<input type="radio" id='iritzia' name="gaia" value="11" onclick = "bilaketaEgin()">iritzia
		<input type="radio" id='kirolak' name="gaia" value="12" onclick = "bilaketaEgin()">kirolak
		<input type="radio" id='komunikabideak' name="gaia" value="13" onclick = "bilaketaEgin()">komunikabideak
		<input type="radio" id='kultura' name="gaia" value="14" onclick = "bilaketaEgin()">kultura
		<input type="radio" id='politika' name="gaia" value="15" onclick = "bilaketaEgin()">politika
		<input type="radio" id='publizitatea' name="gaia" value="16" onclick = "bilaketaEgin()">publizitatea
		<input type="radio" id='taldeak' name="gaia" value="17" onclick = "bilaketaEgin()">taldeak
		<input type="radio" id='udala' name="gaia" value="18" onclick = "bilaketaEgin()">udala
		<input type="radio" id='uztarriaeus' name="gaia" value="19" onclick = "bilaketaEgin()">uztarriaeus
		<hr>


	 
		<div id='charts'></div>

		
	</body>
</html>
