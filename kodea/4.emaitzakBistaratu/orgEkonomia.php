
<html>
	<head>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">


			google.load('visualization','1.0',{'packages':['corechart']});
			google.setOnLoadCallback(dibujar);
	
			function dibujar()
			{
			
					var data = new google.visualization.DataTable();
					data.addColumn('string','Ciudad');
					data.addColumn('number','Agerpen Kopurua');
					data.addRows(
								[
									
['Eusko jaurlaritza ',108],
['lab',108],
['*edun',84],
['Gipuzkoako Foru aldundi ',79],
['izan',68],
['Urola erdi ',68],
['udal',67],
['Lana Erregulatzeko txosten ',44],
['hilabete',44],
['Enpresa batzorde ',43],
['corrugados',38],
['Urola Kostako hitza ',36],
['Urola desberdin ',34],
['Bertan Merkatari elkarte ',32],
['Foru aldundi ',32],
['jaurlaritza',31],
['Gallardo talde ',30],
['Urola kosta ',29],
['Merkatari elkarte ',27],
['Iraurgi jmujika@iraurgiberritzen.eus ',24],
['baieztatu',24],
['Gallardo taldea ',23],
['Corrugados azpeitia ',22],
['Espainiako Auzitegi nazional ',22],
['fagor',21],
['Azpeitiko udal ',21],
['zesto',21],
['ela',21],
['ehne',19],
['Urola erdia ',19],
['zestoa',18],
['Urola Kostako hitz ',18],
['urrestilla',18],
['euskadi',17],
['Loiola Berrikuntza fundazio ',17],
['jesus',16],
['Iraurgi berritzen ',16],
['bertan',16],
['Espainiako gobernu ',15],
['Enpresa batzordeko ',15],
['grumal',15],
['ccoo',14],
['let',14],
['Iraurgi lantzen ',14],
['udaletxe',14],
['pdf',14],
['itsasertza',13],
['fundazio',13],
['ugt',13],
['bestelako',12],
['Garapeneko Zuzendari nagusi ',12],
['AIEK sistema ',12],
['labeko',12],
['bulebar',12],
['Landa Garapen programa ',12],
['aureli',12],
['Europar politiketa ',12],
['Eskualdeen txo ',12],
['Bertan Merkataritza elkar ',12],
['Explore San Sebastian region ',12],
['silvesr',12],
['Sustapen plan ',12],
['Corrugados lasao ',11],
['aldundi',11],
['ukan',11],
['Sindikatu zahar ',11],
['Alfonso Gallardo talde ',10],
['bbva',10],
['eaj',10],
['egon',10],
['Urola Kostako hitzaldi ',10],
['Azpeitia txartelarekin ',10],
['enpresa',10],
['Rural kutxa ',10],
['Urola Erdiko Enplegu plan ',10],
['Fagor etxetresna ',10],
['dela-eta',10],
['adegi',9],
['Lan delegaritza ',9],
['Lan delegaritz ',9],
['eilas',9],
['sl',9],
['Azpeitiko Bertan Merkatari elkarte ',9],
['Urola Kostako lab ',8],
['horrela',8],
['Gipuzkoako Foru aldundia ',8],
['Lan otsail ',8],
['Bertan Azpeitiko Merkatari elkarte ',8],
['eskubide',8],
								]
								);
var opciones = {'title':'',
									'width':1500*2,
									'height':500};

					var grafica = new google.visualization.ColumnChart(document.getElementById('charts'));
					grafica.draw(data,opciones);

				
		}

		</script>
				<script type="text/javascript">
			
		function bilaketaEgin(){
		
			if(document.getElementById('denakType').checked) {

				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'denakTypeDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'denakTypeAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'denakTypeAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'denakTypeBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'denakTypeEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'denakTypeEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'denakTypeFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'denakTypeGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'denakTypeHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'denakTypeHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'denakTypeHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'denakTypeIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'denakTypeKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'denakTypeKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'denakTypeKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'denakTypePolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'denakTypePublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'denakTypeTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'denakTypeUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'denakTypeUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('loc').checked) {

				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'locDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'locAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'locAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'locBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'locEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'locEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'locFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'locGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'locHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'locHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'locHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'locIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'locKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'locKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'locKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'locPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'locPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'locTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'locUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'locUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('org').checked) {

				
				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'orgDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'orgAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'orgAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'orgBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'orgEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'orgEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'orgFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'orgGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'orgHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'orgHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'orgHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'orgIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'orgKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'orgKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'orgKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'orgPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'orgPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'orgTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'orgUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'orgUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('per').checked) {

				
				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'perTypeDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'perAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'perAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'perBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'perEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'perEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'perFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'perGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'perHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'perHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'perHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'perIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'perKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'perKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'perKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'perPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'perPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'perTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'perUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'perUztarriaeus.php'; 
				}
			}
		}

		</script>

	<title>Uztarriako Entitateen Estadistikak</title>

	</head>
	<body>
<h1 align="center">Uztarriako Entitate Izendunen Grafikoak</h1>
		<p align="center">Uztarriak 2001etik 2017rarte beraien weborrian argitaratutako 17034 artikulu prozesatu dira.<br>
						  Grafikoak, entitate moten arabera eta gaien arabera banatuta daude  </p>
		<hr>
		<input type="radio" id='denakType' name="type" value="1" onclick = "bilaketaEgin()">Denak
		<input type="radio" id='loc' name="type" value="2" onclick = "bilaketaEgin()">Lekuak
		<input type="radio" id='org' name="type" value="3" onclick = "bilaketaEgin()">Antolakundeak
		<input type="radio" id='per' name="type" value="4" onclick = "bilaketaEgin()">Pertsonak<br> 
		<hr>
		<input type="radio" id='denakGaia' name="gaia" value="1" onclick = "bilaketaEgin()">Denak
		<input type="radio" id='aisia' name="gaia" value="1" onclick = "bilaketaEgin()">Aisia
		<input type="radio" id='azoka-herritik' name="gaia" value="2" onclick = "bilaketaEgin()">azoka-herritik
		<input type="radio" id='besterik' name="gaia" value="3" onclick = "bilaketaEgin()">besterik
		<input type="radio" id='ekonomia' name="gaia" value="4" onclick = "bilaketaEgin()">ekonomia
		<input type="radio" id='euskara' name="gaia" value="5" onclick = "bilaketaEgin()">euskara
		<input type="radio" id='festak' name="gaia" value="6" onclick = "bilaketaEgin()">festak
		<input type="radio" id='gizartea' name="gaia" value="7" onclick = "bilaketaEgin()">gizartea
		<input type="radio" id='herria' name="gaia" value="8" onclick = "bilaketaEgin()">herria
		<input type="radio" id='hezkuntza' name="gaia" value="9" onclick = "bilaketaEgin()">hezkuntza<br>
		<input type="radio" id='hirigintza' name="gaia" value="10" onclick = "bilaketaEgin()">hirigintza
		<input type="radio" id='iritzia' name="gaia" value="11" onclick = "bilaketaEgin()">iritzia
		<input type="radio" id='kirolak' name="gaia" value="12" onclick = "bilaketaEgin()">kirolak
		<input type="radio" id='komunikabideak' name="gaia" value="13" onclick = "bilaketaEgin()">komunikabideak
		<input type="radio" id='kultura' name="gaia" value="14" onclick = "bilaketaEgin()">kultura
		<input type="radio" id='politika' name="gaia" value="15" onclick = "bilaketaEgin()">politika
		<input type="radio" id='publizitatea' name="gaia" value="16" onclick = "bilaketaEgin()">publizitatea
		<input type="radio" id='taldeak' name="gaia" value="17" onclick = "bilaketaEgin()">taldeak
		<input type="radio" id='udala' name="gaia" value="18" onclick = "bilaketaEgin()">udala
		<input type="radio" id='uztarriaeus' name="gaia" value="19" onclick = "bilaketaEgin()">uztarriaeus
		<hr>


	 
		<div id='charts'></div>

		
	</body>
</html>
