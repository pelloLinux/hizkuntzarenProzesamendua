
<html>
	<head>
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">


			google.load('visualization','1.0',{'packages':['corechart']});
			google.setOnLoadCallback(dibujar);
	
			function dibujar()
			{
			
					var data = new google.visualization.DataTable();
					data.addColumn('string','Ciudad');
					data.addColumn('number','Agerpen Kopurua');
					data.addRows(
								[
									['Julian eizmendi ',18],
['Mikel ibarzabal ',16],
['Jose Mari bastida ',14],
['Jose artetxe ',13],
['Juan Bautista mendizabal ',13],
['Eneko etxeberria ',13],
['Santo tomas ',10],
['azpeitia',10],
['ibarzabal',9],
['Aitor unanue ',8],
['Josu labaka ',8],
['madalena',8],
['landeta',8],
['Aitor bereziartua ',8],
['Ildefonso gurrutxaga ',8],
['agirre',8],
['unanue',7],
['basazabal',7],
['beristain',7],
['mendizabal',7],
['Karmelo etxegarai ',7],
['Miren odriozola ',7],
['Gaizka larrañaga ',7],
['Agirre elorza ',7],
['aitonena',7],
['esklaba',6],
['etxeberri',6],
['Joxe Urbieta takolo ',6],
['sanjuandegi',6],
['Jose Luis frantzesena ',6],
['jesus',6],
['kortatxo',6],
['gorrotxategi',6],
['Leire etxaniz ',6],
['Joxe Antonio salegi ',6],
['Zuhaitz eguna ',6],
['salegi',6],
['Joxe Mari gorrotxategi ',6],
['Josu izagir ',6],
['Alaitz aguado ',6],
['Sanjuandegiko beristain ',6],
['Done jakue ',6],
['Josu izagirre ',6],
['Aitor bereziartu ',6],
['urbitarte',6],
['izagirre',6],
['Balentina agirre ',6],
['zarautz-donostia',6],
['labaka',6],
['sara-kanbo',6],
['etxeberria',5],
['Jose Maria bastida ',5],
['eizmendi',5],
['Maite fran ',5],
['Izaskun egiguren ',5],
['Imanol elias ',5],
['Manuel unanue ',5],
['iraurgi',4],
['Nerea uranga ',4],
['rezabal',4],
['Sebastian lizaso ',4],
['San martin ',4],
['Xabier rezabal ',4],
['egiguren',4],
['Perez arregi ',4],
['Maider olaizola ',4],
['Beñat lizaso ',4],
['Leire ostolaza ',4],
['Eneko ibarguren ',4],
['aiton',4],
['Julian elorza ',4],
['Jose Luis otamendi ',3],
['Aitor gorrotxategi ',3],
['Aitzol iparragirre ',3],
['Iñaki errazkin ',3],
['Damaso azkue ',3],
['Juan antxieta ',3],
['Ainara epelde ',3],
['Juan Joxe agir ',3],
['Azpeitia lantzen ',3],
['Iker salegi ',3],
['maider',3],
['arana',3],
['Sebastian donea ',3],
['artetxe',3],
['Julian barrenetxea ',3],
['santatu',3],
['imaz',3],
['Evaristo Fernandez Rodriguez 13-francisca ',3],

								]
								);
var opciones = {'title':'',
									'width':1500*2,
									'height':500};

					var grafica = new google.visualization.ColumnChart(document.getElementById('charts'));
					grafica.draw(data,opciones);

				
		}

		</script>
				<script type="text/javascript">
			
		function bilaketaEgin(){
		
			if(document.getElementById('denakType').checked) {

				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'denakTypeDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'denakTypeAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'denakTypeAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'denakTypeBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'denakTypeEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'denakTypeEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'denakTypeFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'denakTypeGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'denakTypeHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'denakTypeHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'denakTypeHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'denakTypeIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'denakTypeKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'denakTypeKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'denakTypeKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'denakTypePolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'denakTypePublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'denakTypeTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'denakTypeUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'denakTypeUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('loc').checked) {

				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'locDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'locAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'locAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'locBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'locEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'locEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'locFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'locGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'locHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'locHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'locHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'locIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'locKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'locKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'locKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'locPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'locPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'locTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'locUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'locUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('org').checked) {

				
				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'orgDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'orgAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'orgAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'orgBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'orgEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'orgEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'orgFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'orgGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'orgHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'orgHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'orgHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'orgIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'orgKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'orgKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'orgKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'orgPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'orgPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'orgTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'orgUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'orgUztarriaeus.php'; 
				}
			}

			else if(document.getElementById('per').checked) {

				
				if(document.getElementById('denakGaia').checked) {
					window.location.href = 'perTypeDenakGaia.php'; 
				}
				else if(document.getElementById('aisia').checked) {
					window.location.href = 'perAisia.php'; 
				}
				else if(document.getElementById('azoka-herritik').checked) {
					window.location.href = 'perAzokaHerritik.php'; 
				}
				else if(document.getElementById('besterik').checked) {
					window.location.href = 'perBestetik.php'; 
				}
				else if(document.getElementById('ekonomia').checked) {
					window.location.href = 'perEkonomia.php'; 
				}
				else if(document.getElementById('euskara').checked) {
					window.location.href = 'perEuskara.php'; 
				}
				else if(document.getElementById('festak').checked) {
					window.location.href = 'perFestak.php'; 
				}
				else if(document.getElementById('gizartea').checked) {
					window.location.href = 'perGizartea.php'; 
				}
				else if(document.getElementById('herria').checked) {
					window.location.href = 'perHerria.php'; 
				}
				else if(document.getElementById('hezkuntza').checked) {
					window.location.href = 'perHezkuntza.php'; 
				}
				else if(document.getElementById('hirigintza').checked) {
					window.location.href = 'perHirigintza.php'; 
				}
				else if(document.getElementById('iritzia').checked) {
					window.location.href = 'perIritzia.php'; 
				}
				else if(document.getElementById('kirolak').checked) {
					window.location.href = 'perKirolak.php'; 
				}
				else if(document.getElementById('komunikabideak').checked) {
					window.location.href = 'perKomunikabideak.php'; 
				}
				else if(document.getElementById('kultura').checked) {
					window.location.href = 'perKultura.php'; 
				}
				else if(document.getElementById('politika').checked) {
					window.location.href = 'perPolitika.php'; 
				}
				else if(document.getElementById('publizitatea').checked) {
					window.location.href = 'perPublizitatea.php'; 
				}
				else if(document.getElementById('taldeak').checked) {
					window.location.href = 'perTaldeak.php'; 
				}
				else if(document.getElementById('udala').checked) {
					window.location.href = 'perUdala.php'; 
				}
				else if(document.getElementById('uztarriaeus').checked) {
					window.location.href = 'perUztarriaeus.php'; 
				}
			}
		}

		</script>

	<title>Uztarriako Entitateen Estadistikak</title>

	</head>
	<body>
<h1 align="center">Uztarriako Entitate Izendunen Grafikoak</h1>
		<p align="center">Uztarriak 2001etik 2017rarte beraien weborrian argitaratutako 17034 artikulu prozesatu dira.<br>
						  Grafikoak, entitate moten arabera eta gaien arabera banatuta daude  </p>
		<hr>
		<input type="radio" id='denakType' name="type" value="1" onclick = "bilaketaEgin()">Denak
		<input type="radio" id='loc' name="type" value="2" onclick = "bilaketaEgin()">Lekuak
		<input type="radio" id='org' name="type" value="3" onclick = "bilaketaEgin()">Antolakundeak
		<input type="radio" id='per' name="type" value="4" onclick = "bilaketaEgin()">Pertsonak<br> 
		<hr>
		<input type="radio" id='denakGaia' name="gaia" value="1" onclick = "bilaketaEgin()">Denak
		<input type="radio" id='aisia' name="gaia" value="1" onclick = "bilaketaEgin()">Aisia
		<input type="radio" id='azoka-herritik' name="gaia" value="2" onclick = "bilaketaEgin()">azoka-herritik
		<input type="radio" id='besterik' name="gaia" value="3" onclick = "bilaketaEgin()">besterik
		<input type="radio" id='ekonomia' name="gaia" value="4" onclick = "bilaketaEgin()">ekonomia
		<input type="radio" id='euskara' name="gaia" value="5" onclick = "bilaketaEgin()">euskara
		<input type="radio" id='festak' name="gaia" value="6" onclick = "bilaketaEgin()">festak
		<input type="radio" id='gizartea' name="gaia" value="7" onclick = "bilaketaEgin()">gizartea
		<input type="radio" id='herria' name="gaia" value="8" onclick = "bilaketaEgin()">herria
		<input type="radio" id='hezkuntza' name="gaia" value="9" onclick = "bilaketaEgin()">hezkuntza<br>
		<input type="radio" id='hirigintza' name="gaia" value="10" onclick = "bilaketaEgin()">hirigintza
		<input type="radio" id='iritzia' name="gaia" value="11" onclick = "bilaketaEgin()">iritzia
		<input type="radio" id='kirolak' name="gaia" value="12" onclick = "bilaketaEgin()">kirolak
		<input type="radio" id='komunikabideak' name="gaia" value="13" onclick = "bilaketaEgin()">komunikabideak
		<input type="radio" id='kultura' name="gaia" value="14" onclick = "bilaketaEgin()">kultura
		<input type="radio" id='politika' name="gaia" value="15" onclick = "bilaketaEgin()">politika
		<input type="radio" id='publizitatea' name="gaia" value="16" onclick = "bilaketaEgin()">publizitatea
		<input type="radio" id='taldeak' name="gaia" value="17" onclick = "bilaketaEgin()">taldeak
		<input type="radio" id='udala' name="gaia" value="18" onclick = "bilaketaEgin()">udala
		<input type="radio" id='uztarriaeus' name="gaia" value="19" onclick = "bilaketaEgin()">uztarriaeus
		<hr>


	 
		<div id='charts'></div>

		
	</body>
</html>
