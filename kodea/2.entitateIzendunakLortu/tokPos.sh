#!/bin/sh

mkdir testuak/ixaPipes

#ixa-pipe-tok: tokenizatzailea.
#ixa-pipe-pos: etiketatzailea (POS tagger-a eta lematizatzailea).

k="1"

for i in $(ls testuak/originalak)
do
	echo "********"
	echo "** $k **"
	echo "********"

	cat testuak/originalak/$i | java -jar tresnak/ixa-pipes-1.1.1/ixa-pipe-tok-1.8.5-exec.jar tok -l eu | java -jar tresnak/ixa-pipes-1.1.1/ixa-pipe-pos-1.5.1-exec.jar client -p 2100 > testuak/ixaPipes/$i.NAF

	k=$(($k+1))

done


