package nagusia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class nagusia {

	public static void main(String[] args) {
		// TODO Auto-generated method stub


		String url;
		String[] gaiak = {"aisia","azoka-herritik","besterik","ekonomia","euskara","festak","gizartea","herria","hezkuntza","hirigintza","iritzia","kirolak","komunikabideak","kultura","politika","publizitatea","taldeak","udala","uztarriaeus"};
		for(int i=0; i<gaiak.length; i++)
		{
			System.out.println("***** "+gaiak[i]+" *****");
			url = "http://uztarria.eus/"+gaiak[i];
			gaiBakoitzekoakLortu(url,gaiak[i]);
		}

		System.exit(0);
	}

	private static void gaiBakoitzekoakLortu(String url,String gaia)
	{
		System.out.println("::: page1 :::");
		int kop = pageKop(url);
		//hasierako orriakoak lortu
		LinkedList<String> artikuluenEnlazeak = artikuluenEnlazeakLortu(url);
		fitxategianIdatzi(artikuluenEnlazeak, gaia);

		//dauden orri guztiak pasa
		for(int i=2; i<kop; i++)
		{
			System.out.println("::: page"+i+" :::");
			artikuluenEnlazeak = artikuluenEnlazeakLortu(url+"?page="+i);
			fitxategianIdatzi(artikuluenEnlazeak, gaia);

		}		
	}


	private static String sendQuery(String query) throws IOException{

		/** HTML kodea lortu, URL-a emanda */

		BufferedReader bufferedReader = null;

		String result="";
		URL url = new URL(query);
		HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();
		if(httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK)
		{
			InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
			bufferedReader = new BufferedReader(inputStreamReader,8192);
		}

		String line = null;
		while((line = bufferedReader.readLine()) != null)result += line+"\n";

		bufferedReader.close();
		return result;
	}


	//ustarriak igotako enlazeak soilik lortu
	private static LinkedList<String> artikuluenEnlazeakLortu(String url)
	{

		try{
			LinkedList<String> linkak = new LinkedList<String>();
			Document doc = Jsoup.connect(url).get();
			org.jsoup.select.Elements links = doc.select("a");
			int j=0;
			for(Element e : links)
			{//hasiera orriko link guztiak lortu.		
				String lag = e.attr("abs:href");
				if(lag.split("aktualitatea").length>1 && lag.indexOf("artxiboa")==-1 && lag.indexOf("..")==-1 &&  lag.split("#").length<2 && linkak.indexOf(lag)==-1)
				{
					linkak.add(lag);
				}

			}
			return linkak;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private static int pageKop(String url)
	{

		try{

			Document doc = Jsoup.connect(url).get();
			org.jsoup.select.Elements links = doc.select("a");
			int handiena=0;
			for(Element e : links)
			{//hasiera orriko link guztiak lortu.		
				String lag = e.attr("abs:href");
				String[] page = lag.split("page=");

				if(page.length>1)
				{
					if(handiena < Integer.parseInt(page[1]))
						handiena = Integer.parseInt(page[1]);
				}

			}
			return handiena;


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return -1;
	}


	private static void fitxategianIdatzi(LinkedList<String> eskuakLinks, String gaia)
	{
		try{

			PrintWriter out;
			for (int i = 0; i < eskuakLinks.size(); i++) {

				String html = sendQuery(eskuakLinks.get(i));
				//System.out.println(html);
				Document d = Jsoup.parse(html);
				System.out.print("*");
				String lag = d.body().text();//+"###"+quitarSaltos(d.getElementsByAttribute("alt").toString());
				String[] egunaEtaTestua = getEgunaEtaTestua(lag);
				out = new PrintWriter("fitxategiak/"+gaia+"_"+egunaEtaTestua[0]+"_"+i+".txt");
				out.println(egunaEtaTestua[1]);
				out.close();
			}
			System.out.println();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String[] getEgunaEtaTestua(String s)
	{
		String lag = "";
		String eguna;
		if(s.split("Artikulua ").length>1)
		{
			lag = s.split("Artikulua ")[1];
			
		}
		else if(s.split("Argazki galeria ").length>1)
		{
			lag = s.split("Argazki galeria ")[1];
		}
		else if(s.split("Argazkia ").length>1)
		{
			lag = s.split("Argazkia ")[1];
		}
		else if(s.split("Bideoa ").length>1)
		{
			lag = s.split("Bideoa ")[1];
		}
		eguna = lag.split(" ")[0];
		String ordua =  lag.split(" ")[1];
		lag = lag.split(ordua+" ")[1];
		//falloa badago ikusteko
		if(lag.compareTo("")==0)System.out.println(s);
		//alperrikakoa kendu
		if(lag.split("Bidali").length>1)
			lag = lag.split("Bidali")[0];
		lag = lag.replace("Ikusi haundiago | Argazki originala", "");
		
		
				
		String[] r = {eguna,lag};
		return r;
		
	}

}
